import { Component,NgZone, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, from } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
//import {showResult} from './../assets/script.js'
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { SidebarComponent } from '@syncfusion/ej2-angular-navigations';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address} from 'ngx-google-places-autocomplete/objects/address';
//import { google } from '@agm/core/services/google-maps-types';
 


@Component({
  selector: 'app-maphome',
  templateUrl: './maphome.component.html',
  styleUrls: ['./maphome.component.css']
})
export class MaphomeComponent implements OnInit {
  title = 'AmbulanceBooking';

  showModal: boolean;
  registerForm: FormGroup;
  submitted = false;
  private geoCoder;
  lat: number;
  lng: number;
  zoom: number=15;
  address: string;
  SendNotification:FormGroup;
  output:boolean=false;
  origin = { lat: 24.799448, lng: 120.979021 };
  destination = { lat: 24.799524, lng: 120.975017 };
  location="./../../assets/ambulance1.png"
  // private map: google.maps.Map = null;
  // private heatmap: google.maps.visualization.HeatmapLayer = null;
  directionsRenderer;
  directionsService ;
  mapOptions;

  @ViewChild('ngOtpInput') ngOtpInputRef:any;//Get reference using ViewChild and the specified hash
yourMethod(){
  this.ngOtpInputRef.setValue("okay");//yourvalue can be any string or number eg. 1234 or '1234'
}
onOtpChange(event)
{}
  @ViewChild('search')
  public searchElementRef: ElementRef;
  options={
    types: []
    }
  @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  
  waypoints = [
     {location: { lat: 39.0921167, lng: -94.8559005 }},
     {location: { lat: 41.8339037, lng: -87.8720468 }}
  ];
  public handleAddressChange(address: Address) {
    console.log(address.place_id);
    console.log(address);
    this.origin={'lat':address.geometry.location.lat(),'lng':address.geometry.location.lng()};
    this.lat = address.geometry.location.lat();
   this.lng = address.geometry.location.lng();
    
  // Do some stuff
}
public handleDestinationAddressChange(address: Address) {
  console.log(address.place_id);
  console.log(address);
  this.destination={'lat':address.geometry.location.lat(),'lng':address.geometry.location.lng()};
  this.lat = address.geometry.location.lat();
 this.lng = address.geometry.location.lng();
  
// Do some stuff
}

  constructor(private ngZone: NgZone,private mapsAPILoader: MapsAPILoader,private formBuilder:FormBuilder){
    // if (navigator)
    // {
    // navigator.geolocation.getCurrentPosition( pos => {
    //     this.lng = +pos.coords.longitude;
    //     this.lat = +pos.coords.latitude;
    //   });
    // }
    this.lat= 51.673858;
      this.lng=7.815982;
    this.directionsService = new google.maps.DirectionsService;
    this.SendNotification = this.formBuilder.group({
      Notificationtype: ['',Validators.required], 
      EventDate:['',Validators.required],
      Description:['',Validators.required],
});
  }

  show() {
  this.showModal = true; // Show-Hide Modal Check

   }
  onChange(value: string) {
    
  }
//   onMapLoad(mapInstance: google.maps.Map) {
//   //  this.map = mapInstance;  

//     // here our in other method after you get the coords; but make sure map is loaded

//     // const coords: google.maps.LatLng[] =  // can also be a google.maps.MVCArray with LatLng[] inside    
//     // this.heatmap = new google.maps.visualization.HeatmapLayer({
//     //     map: this.map,
//     //     data: coords
//    // });
//    let trafficLayer = new google.maps.TrafficLayer();
//    trafficLayer.setMap(mapInstance);
// }
  //Bootstrap Modal Close event
  hide() {
    this.showModal = false;
  }
  ngOnInit() {
   // this.mapsAPILoader.load().then(() => {
      // this.setCurrentLocation();
    // this.geoCoder = new google.maps.Geocoder;

    //   let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
    //   autocomplete.addListener("place_changed", () => {
    //     this.ngZone.run(() => {
    //       //get the place result
    //       let place: google.maps.places.PlaceResult = autocomplete.getPlace();

    //       //verify result
    //       if (place.geometry === undefined || place.geometry === null) {
    //         return;
    //       }

    //       //set latitude, longitude and zoom
    //       this.lat = place.geometry.location.lat();
    //       this.lng = place.geometry.location.lng();
    //       this.zoom = 12;
    //     });
    //   });
    // });
    
 // }
  // private setCurrentLocation() {
  //   if ('geolocation' in navigator) {
  //     navigator.geolocation.getCurrentPosition((position) => {
  //       this.lat = position.coords.latitude;
  //       this.lng = position.coords.longitude;
  //       this.zoom = 8;
  //       this.getAddress(this.lat, this.lng);
  //     });
  //   }
   // });
 // this.getAddress(51.723858, 7.895982);
 this.getDirection();
}
 
getDirection() {
  this.origin = { lat: 24.799448, lng: 120.979021 };
  this.destination = { lat: 24.799524, lng: 120.975017 };
}
  getAddress(latitude:number, longitude:number) {
    console.log(latitude,longitude)
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
          console.log(this.address);
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    if (this.submitted) {
      this.showModal = false;
    }

  }
  

  LogOut()
  {
    localStorage.clear();
    location.reload();
  }
 

  // set(lat:number,lng:number)
  // {
    
  //   this.marks.lat=lat;
  //   this.marks.lng=lng;

  // }
  onSendNotification()
  {
    this.submitted = true;
    if (this.SendNotification.invalid) {
      console.log("error");
      return;
    }
    else 
    {
      this.output=this.SendNotification.value;
    console.log(this.output);
    }
    if (this.submitted) {
      this.SendNotification.reset();
    }
  }
 public calculateAndDisplayRoute() {
    var waypts = [];
    var checkboxArray  = document.getElementById('waypoints');
    // for (var i = 0; i < checkboxArray; i++) {
    //   if (checkboxArray.options[i].selected) {
    //     waypts.push({
    //       location: checkboxArray[i].value,
    //       stopover: true
    //     });
    //   }
    // }
    this.directionsService.route({
      origin: {lat: this.origin.lat, lng: this.origin.lng},  // Haight.
      destination: {lat: this.destination.lat, lng: this.destination.lng},
      
      //waypoints: waypts,
      optimizeWaypoints: true,
      provideRouteAlternatives: true,
      avoidTolls: true,
      travelMode: 'DRIVING'
    }, function(response, status) {
      if (status === 'OK') {
        this.directionsRenderer = new google.maps.DirectionsRenderer;

        this.directionsRenderer.setDirections(response);
        var route = response.routes[0];
        console.log(route);

        var summaryPanel = document.getElementById('directions-panel');
        summaryPanel.innerHTML = '';
        // For each route, display summary information.
        for (var i = 0; i < route.legs.length; i++) {
          var routeSegment = i + 1;
          summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
              '</b><br>';
          summaryPanel.innerHTML +='<b>Source: </b>'+ route.legs[i].start_address + ' <br> ';
          summaryPanel.innerHTML +='<b>Destination: </b>'+ route.legs[i].end_address + '<br>';
          summaryPanel.innerHTML += '<b>Distance: </b>'+route.legs[i].distance.text + '<br>';
           summaryPanel.innerHTML +='<b>Duration: </b>'+ route.legs[i].duration.text + '<br><br>';

        }
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }
  markers: marker[] = [
    {
      lat: 51.673858,
      lng: 7.815982,
      label: 'A',
      draggable: true
    },
    {
      lat: 51.373858,
      lng: 7.215982,
      label: 'B',
      draggable: false
    },
    {
      lat: 51.723858,
      lng: 7.895982,
      label: 'C',
      draggable: true
    }
  ]
  public openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("map").style.marginLeft = "250px";
  }
  
  public closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("map").style.marginLeft= "0";
  }
  public GetProfile()
  {
       
  }
  public Order()
  {
    alert("okay"); 
  }
}
interface marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}


