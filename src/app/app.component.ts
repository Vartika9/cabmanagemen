import { Component,NgZone, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, from } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import {showResult} from './../assets/script.js'
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { google } from '@agm/core/services/google-maps-types';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent {
  title = 'AmbulanceBooking';

//   showModal: boolean;
//   registerForm: FormGroup;
//   submitted = false;
//   private geoCoder;
//   lat: number;
//   lng: number;
//   zoom: number=15;
//   address: string;
  
//   @ViewChild('search')
//   public searchElementRef: ElementRef;
  

//   constructor(private ngZone: NgZone,private mapsAPILoader: MapsAPILoader){
//     if (navigator)
//     {
//     navigator.geolocation.getCurrentPosition( pos => {
//         this.lng = +pos.coords.longitude;
//         this.lat = +pos.coords.latitude;
//       });
//     }
//   }
//   private formBuilder: FormBuilder

//   // constructor(private formBuilder: FormBuilder) { }
//   show() {
//   this.showModal = true; // Show-Hide Modal Check

//    }
//   onChange(value: string) {
    
//   }
 
//   //Bootstrap Modal Close event
//   hide() {
//     this.showModal = false;
//   }
//   ngOnInit() {
//     this.registerForm = this.formBuilder.group({
//       email: ['', [Validators.required, Validators.email]],
//       password: ['', [Validators.required, Validators.minLength(6)]]
//     });
//    // this.mapsAPILoader.load().then(() => {
//      // this.setCurrentLocation();
//    //  this.geoCoder = new google.maps.Geocoder;

//     //   let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
//     //   autocomplete.addListener("place_changed", () => {
//     //     this.ngZone.run(() => {
//     //       //get the place result
//     //       let place: google.maps.places.PlaceResult = autocomplete.getPlace();

//     //       //verify result
//     //       if (place.geometry === undefined || place.geometry === null) {
//     //         return;
//     //       }

//     //       //set latitude, longitude and zoom
//     //       this.lat = place.geometry.location.lat();
//     //       this.lng = place.geometry.location.lng();
//     //       this.zoom = 12;
//     //     });
//     //   });
//     // });
    
//  // }
//   // private setCurrentLocation() {
//   //   if ('geolocation' in navigator) {
//   //     navigator.geolocation.getCurrentPosition((position) => {
//   //       this.lat = position.coords.latitude;
//   //       this.lng = position.coords.longitude;
//   //       this.zoom = 8;
//   //       this.getAddress(this.lat, this.lng);
//   //     });
//   //   }
//   }
//   getAddress(latitude:number, longitude:number) {
//     this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
//       console.log(results);
//       console.log(status);
//       if (status === 'OK') {
//         if (results[0]) {
//           this.zoom = 12;
//           this.address = results[0].formatted_address;
//         } else {
//           window.alert('No results found');
//         }
//       } else {
//         window.alert('Geocoder failed due to: ' + status);
//       }

//     });
//   }

//   // convenience getter for easy access to form fields
//   get f() { return this.registerForm.controls; }
//   onSubmit() {
//     this.submitted = true;
//     // stop here if form is invalid
//     if (this.registerForm.invalid) {
//       return;
//     }
//     if (this.submitted) {
//       this.showModal = false;
//     }

//   }
  

 
 

//   // set(lat:number,lng:number)
//   // {
    
//   //   this.marks.lat=lat;
//   //   this.marks.lng=lng;

//   // }
//   markers: marker[] = [
//     {
//       lat: 51.673858,
//       lng: 7.815982,
//       label: 'A',
//       draggable: true
//     },
//     {
//       lat: 51.373858,
//       lng: 7.215982,
//       label: 'B',
//       draggable: false
//     },
//     {
//       lat: 51.723858,
//       lng: 7.895982,
//       label: 'C',
//       draggable: true
//     }
//   ]

// }
// interface marker {
// 	lat: number;
// 	lng: number;
// 	label?: string;
// 	draggable: boolean;
}

