import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent  } from "./login/login.component";
import { MaphomeComponent } from "./maphome/maphome.component";
import { NavComponent } from "./nav/nav.component";
import { RidesComponent } from "././rides/rides.component";
const routes: Routes = [
  { path: '', redirectTo: '/notify', pathMatch: 'full' },
  {
    path:'notify',
    component:LoginComponent
  },
  {
    path:'notify/home',
    component:MaphomeComponent
  },
  {
    path:'notify/rides',
    component:RidesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
