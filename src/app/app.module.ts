import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MatToolbarModule } from '@angular/material/toolbar';


import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion';
//import { MatDatepickerModule } from '@angular/material/datepicker';
import { Routes, RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from "@angular/common/http";
//import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';


import { LoginComponent } from './login/login.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {FormsModule}from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatTabsModule} from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { HttpModule } from '@angular/http';
import { SidebarModule } from '@syncfusion/ej2-angular-navigations';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxPaginationModule } from 'ngx-pagination';

//import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MaphomeComponent } from "./maphome/maphome.component";
import { AgmDirectionModule } from 'agm-direction';
import { NgOtpInputModule } from  'ng-otp-input';
import { RidesComponent } from './rides/rides.component';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    MaphomeComponent,
    LoginComponent,
    RidesComponent
  
  ],
  imports: [
    BrowserModule,
     MatToolbarModule,
     LayoutModule,
     MatButtonModule,
     MatSidenavModule,
     MatIconModule,
     MatListModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatExpansionModule,
    HttpClientModule,
    AppRoutingModule,
    HttpModule,
    MatMenuModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatCardModule,
    FormsModule,
    MatInputModule,
    MatTabsModule,
    SidebarModule,
    GooglePlaceModule,
    AgmDirectionModule ,
    NgOtpInputModule,
    NgxPaginationModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBcLp4r5WEDgHTv31BNHXJIQgSwxCc4qMk',
      libraries: ['visualization'],
  }),
    // MatGoogleMapsAutocompleteModule,
    // AgmCoreModule.forRoot()
   
  ],
  providers: [],
  bootstrap: [AppComponent],

})
export class AppModule { }
