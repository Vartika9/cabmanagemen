import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationServiceService } from "./../Services/notification-service.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  registerForm:FormGroup;
  LoginForm:FormGroup;
  submitted:boolean=false;
  constructor(private formBuilder:FormBuilder,private services:NotificationServiceService) {
    this.services.confirmAuth();

    this.registerForm = this.formBuilder.group({
      FullName: ['',Validators.required],
      Email:['',[Validators.required,Validators.email]],
      PhoneNumber:['',[Validators.required,Validators.minLength(10),Validators.maxLength(10)]],
      Password:['',[Validators.required,Validators.minLength(6)]],
     

});
this.LoginForm = this.formBuilder.group({
  Email: ['',[Validators.required,Validators.email]],
  Password:['',[Validators.required,Validators.minLength(8)]],
});
   }

  ngOnInit(): void {
  }
  //Login
  onSubmit() {
    this.submitted = true;
    console.log(this.LoginForm.value);
    if (this.LoginForm.invalid) {
      return;
    }
    else {
     this.services.signin(this.LoginForm.value);
    }
    if (this.submitted) {
      this.LoginForm.reset();
    }

  }

  //Register User
  onRegister() {
    this.submitted = true;
    console.log(this.registerForm.value);
    if (this.registerForm.invalid) {
      return;
    }
    else {
     this.services.signup(this.registerForm.value);
    }
    if (this.submitted) {
      this.registerForm.reset();
    }

  }
}
