import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from './User';
import {  Router, ActivatedRoute } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class NotificationServiceService {
  sendNotification(Email: JSON)
  {
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
    const httpOptions = {
      headers: headers_object,
    };
   
    this.http.post("https://localhost:44393/notificationhome/sendnotification",Email,httpOptions ).subscribe(
      (result:any) => { 
        console.log(result);
        alert(result.message);
          },
      (error) => { 
       alert(error.error.message); 
       console.error(error);

     }
      );
  }
  createNotification(notification: JSON)
   {
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
    const httpOptions = {
      headers: headers_object,
    };
   
    this.http.post("https://localhost:44393/notificationhome/createnotification",notification,httpOptions ).subscribe(
      (result:any) => { 
        console.log(result);
        alert(result.message);
          },
      (error) => { 
       alert("Error"); 
       console.error(error);

     }
      );
  }
  public currentUser:User = new User();
  constructor(private http: HttpClient,private router:Router,private route:ActivatedRoute) { }
  confirmAuth()
  {
    console.log("okay")
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
    const httpOptions = {
      headers: headers_object,
    };
   
    this.http.get("https://localhost:44393/notificationhome/confirmAuth",httpOptions ).subscribe(
      (result:any) => { 
        console.log(result);
        this.currentUser.email=result[0];
        this.currentUser.fullName=result[1];
        this.currentUser.phoneNumber=result[2];
        this.router.navigate(['notify/home']);
      },
      (error) => { 
        this.router.navigate( ["notify"]);

       console.error(error);

     }
      );

  }
  signup(user:JSON)
 {
  this.http.post("https://localhost:44393/notification/signup", user).subscribe(
    (result:any) => {
      alert(result.message);
      location.reload();
    },
    (error) => { 
      console.error('There was an error!', error); 
    }
   )
 }
 signin(user:JSON) {
  this.http.post("https://localhost:44393/notification/login", user).subscribe(
    (result:any) => {
      console.log(result+result.fullName);
      
      localStorage.setItem("token",result.token);
      if(result.role=="Admin")
       localStorage.setItem("admin",String(true));
      else
       localStorage.setItem("admin",String(false));
      this.router.navigate( ["notify/home"]);
    },
   (error) => { console.error('There was an error!', error); }

  )
 }
}
